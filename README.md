# Tango

Tango 是一个微内核易扩展的Go语言Web框架，他兼有Beego的效率和Martini的中间件设计。

## 最近更新
- [2016-2-1] 新增 session-ssdb，支持将ssdb作为session的后端存储
- [2015-10-23] 更新[renders](https://gitea.com/tango/renders)插件，解决模板修改后需要刷新两次才能生效的问题

## 简介

安装Tango：

    go get gitea.com/lunny/tango

一个经典的Tango例子如下：

```go
package main

import (
    "errors"
    "gitea.com/lunny/tango"
)

type Action struct {
    tango.JSON
}

func (Action) Get() interface{} {
    if true {
        return map[string]string{
            "say": "Hello tango!",
        }
    }
    return errors.New("something error")
}

func main() {
    t := tango.Classic()
    t.Get("/", new(Action))
    t.Run()
}
```

然后在浏览器访问`http://localhost:8000`, 将会得到一个json返回

```Json
{"say":"Hello tango!"}
```

如果将上述例子中的 `true` 改为 `false`, 将会得到一个json返回

```Json
{"err":"something error"}
```

这段代码因为拥有一个内嵌的`tango.JSON`，所以返回值会被自动的转成Json。具体返回可以参见以下文档。

## 特性

- 强大而灵活的路由设计
- 兼容已有的`http.Handler`
- 模块化设计，可以很容易写出自己的中间件
- 高性能的依赖注入方式

## 中间件

中间件让你像AOP编程那样来操作你的Controller。

目前已有很多 [中间件 - gitea.com/tango](https://gitea.com/tango)，可以帮助你快速完成工作:

- [recovery](https://gitea.com/lunny/tango/wiki/ZH_Recovery) - recover after panic
- [compress](https://gitea.com/lunny/tango/wiki/ZH_Compress) - Gzip & Deflate compression
- [static](https://gitea.com/lunny/tango/wiki/ZH_Static) - Serves static files
- [logger](https://gitea.com/lunny/tango/wiki/ZH_Logger) - Log the request & inject Logger to action struct
- [param](https://gitea.com/lunny/tango/wiki/ZH_Params) - get the router parameters
- [return](https://gitea.com/lunny/tango/wiki/ZH_Return) - Handle the returned value smartlly
- [context](https://gitea.com/lunny/tango/wiki/ZH_Context) - Inject context to action struct
- [session](https://gitea.com/tango/session) - [![Build Status](https://drone.io/gitea.com/tango/session/status.png)](https://drone.io/gitea.com/tango/session/latest) [![](http://gocover.io/_badge/gitea.com/tango/session)](http://gocover.io/gitea.com/tango/session) Session manager
- [xsrf](https://gitea.com/tango/xsrf) - [![Build Status](https://drone.io/gitea.com/tango/xsrf/status.png)](https://drone.io/gitea.com/tango/xsrf/latest) [![](http://gocover.io/_badge/gitea.com/tango/xsrf)](http://gocover.io/gitea.com/tango/xsrf) Generates and validates csrf tokens
- [binding](https://gitea.com/tango/binding) - [![Build Status](https://drone.io/gitea.com/tango/binding/status.png)](https://drone.io/gitea.com/tango/binding/latest) [![](http://gocover.io/_badge/gitea.com/tango/binding)](http://gocover.io/gitea.com/tango/binding) Bind and validates forms
- [renders](https://gitea.com/tango/renders) - [![Build Status](https://drone.io/gitea.com/tango/renders/status.png)](https://drone.io/gitea.com/tango/renders/latest) [![](http://gocover.io/_badge/gitea.com/tango/renders)](http://gocover.io/gitea.com/tango/renders) Go template engine
- [dispatch](https://gitea.com/tango/dispatch) - [![Build Status](https://drone.io/gitea.com/tango/dispatch/status.png)](https://drone.io/gitea.com/tango/dispatch/latest) [![](http://gocover.io/_badge/gitea.com/tango/dispatch)](http://gocover.io/gitea.com/tango/dispatch) Multiple Application support on one server
- [tpongo2](https://gitea.com/tango/tpongo2) - [![Build Status](https://drone.io/gitea.com/tango/tpongo2/status.png)](https://drone.io/gitea.com/tango/tpongo2/latest) [![](http://gocover.io/_badge/gitea.com/tango/tpongo2)](http://gocover.io/gitea.com/tango/tpongo2) [Pongo2](https://github.com/flosch/pongo2) teamplte engine support
- [captcha](https://gitea.com/tango/captcha) - [![Build Status](https://drone.io/gitea.com/tango/captcha/status.png)](https://drone.io/gitea.com/tango/captcha/latest) [![](http://gocover.io/_badge/gitea.com/tango/captcha)](http://gocover.io/gitea.com/tango/captcha) Captcha
- [events](https://gitea.com/tango/events) - [![Build Status](https://drone.io/gitea.com/tango/events/status.png)](https://drone.io/gitea.com/tango/events/latest) [![](http://gocover.io/_badge/gitea.com/tango/events)](http://gocover.io/gitea.com/tango/events) Before and After
- [flash](https://gitea.com/tango/flash) - [![Build Status](https://drone.io/gitea.com/tango/flash/status.png)](https://drone.io/gitea.com/tango/flash/latest) [![](http://gocover.io/_badge/gitea.com/tango/flash)](http://gocover.io/gitea.com/tango/flash) Share data between requests
- [debug](https://gitea.com/tango/debug) - [![Build Status](https://drone.io/gitea.com/tango/debug/status.png)](https://drone.io/gitea.com/tango/debug/latest) [![](http://gocover.io/_badge/gitea.com/tango/debug)](http://gocover.io/gitea.com/tango/debug) show detail debug infomaton on log
- [basicauth](https://gitea.com/tango/basicauth) - [![Build Status](https://drone.io/gitea.com/tango/basicauth/status.png)](https://drone.io/gitea.com/tango/basicauth/latest) [![](http://gocover.io/_badge/gitea.com/tango/basicauth)](http://gocover.io/gitea.com/tango/basicauth) basicauth middleware

## 获得帮助

- [Wiki](https://gitea.com/lunny/tango/wiki/ZH_Home)
- [API文档](https://gowalker.org/gitea.com/lunny/tango)
- [中文论坛](https://groups.google.com/forum/#!forum/go-tango)
- [英文论坛](https://groups.google.com/forum/#!forum/go-tango)

## 案例

- [Wego](https://github.com/go-tango/wego)
- [DBWeb](https://github.com/go-xorm/dbweb)
- [Godaily](http://godaily.org) - [github](https://github.com/godaily/news)
- [FXH Blog](https://github.com/gofxh/blog)
- [Gos](https://github.com/go-tango/gos)
- [Gobook](http://gobook.io) - [github](https://github.com/gobook)
