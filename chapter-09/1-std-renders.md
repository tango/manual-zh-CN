# 模板渲染

Renders中间件是一个Go模板引擎的 [Tango](https://gitea.com/lunny/tango) 中间件。

## 安装

    go get gitea.com/tango/renders

## 示例

```Go
type RenderAction struct {
    renders.Renderer
}

func (x *RenderAction) Get() {
    x.Render("test.html", renders.T{
        "test": "test",
    })
}

func main() {
    t := tango.Classic()
    t.Use(renders.New(renders.Options{
        Reload: true,
        Directory: "./templates",
    }))
}
```

## 从一个文件系统接口中读取模板文件

```go
t.Use(renders.New(renders.Options{
        Reload: true,
        Directory: "./templates",
        Filesystem: http.Dir("./templates"),
    }))
```
